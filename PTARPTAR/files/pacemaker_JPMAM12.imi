(************************************************************
 *                         IMITATOR
 *
 * Model of a pacemaker
 *
 * "Modeling and Verification of a Dual Chamber Implantable Pacemaker"
 * Zhihao Jiang, Miroslav Pajic, Salar Moarref, Rajeev Alur, Rahul Mangharam
 * TACAS 2014
 *
 * Author          : Étienne André
 * Created         : 2015/02/11
 * Last modified   : 2021/10/07
 * IMITATOR version: 3.1
 ************************************************************)

var
	(* local clocks *)
	t_AVI, t_LRI, t_PVARP, t_VRP, t_5a,
	(* global clocks *)
	clk, x, x_urgent, global_time,
		: clock;
	TAVI,
	TLRI,
	TPVARP	= 100,
	TVRP		= 150,
	TURI		= 400,
	TPVAB		= 50,
	Aminwait	= 0,(* (random value)*)
	Amaxwait	= 1000,(* (random value)*)

		: parameter;


(************************************************************)
  automaton LRI
(************************************************************)
synclabs: AP, AS, VP, VS;

loc LRI: invariant t_LRI <= TLRI - TAVI
	when True sync VP do {t_LRI := 0} goto LRI;
	when True sync VS do {t_LRI := 0} goto LRI;
	when t_LRI >= TLRI - TAVI sync AP do {t_LRI := 0} goto LRI;
	when True sync AS goto ASed;

loc ASed: invariant True
	when True sync VP do {t_LRI := 0} goto LRI;
	when True sync VS do {t_LRI := 0} goto LRI;

end (*LRI*)



(************************************************************)
  automaton AVI
(************************************************************)
synclabs: AP, AS, VP, VS, AVIac;

loc Idle: invariant True
	when True sync AP do {t_AVI := 0} goto AVI;
	when True sync AS do {t_AVI := 0} goto AVI;

loc AVI: invariant t_AVI <= TAVI
	when True sync VS goto Idle;
	when t_AVI >= TAVI & clk >= TURI sync VP goto Idle;
	when t_AVI >= TAVI & clk < TURI sync AVIac goto WaitURI;

loc WaitURI: invariant clk <= TURI
	when True sync VS goto Idle;
	when clk >= TURI sync VP goto Idle;


end (*AVI*)



(************************************************************)
  automaton URI
(************************************************************)
synclabs: VP, VS;

loc URI: invariant True
	when True sync VP do {clk := 0} goto URI;
	when True sync VS do {clk := 0} goto URI;

end (*URI*)



(************************************************************)
  automaton PVARP
(************************************************************)
synclabs: AR, AS, Aget, VP, VS, PVARPbacktoIdle, PVARPac;

loc Idle: invariant True
	when True sync VP do {t_PVARP := 0} goto PVAB;
	when True sync VS do {t_PVARP := 0} goto PVAB;
	when True sync Aget do {t_PVARP := 0} goto inter;

(* ------------------------------------------------------------ *)
(** COMMITTED **)
loc inter: invariant True
	when True sync AS goto Idle;

loc PVAB: invariant t_PVARP <= TPVAB
	when t_PVARP >= TPVAB sync PVARPac goto PVARP;

loc PVARP: invariant t_PVARP <= TPVARP
	when True sync Aget do {x_urgent := 0} goto inter1;
	when t_PVARP >= TPVARP sync PVARPbacktoIdle goto Idle;

(** COMMITTED **)
(* In fact, urgent is OK because AR is not synchronized with anyone else *)
loc inter1: invariant x_urgent = 0
	when x_urgent = 0 sync AR goto PVARP;


end (*PVARP*)



(************************************************************)
  automaton VRP
(************************************************************)
synclabs: AP, AS, Aget, Vget, VP, VS, PVARPbacktoIdle, VRPac;

loc Idle: invariant True
	when True sync Vget do {t_VRP := 0} goto inter;
	when True sync VP do {t_VRP := 0} goto VRP;
	(* To prevent blocking *)
	when True sync AP goto Idle;
	when True sync AS goto Idle;
	when True sync Aget goto Idle;

(* ------------------------------------------------------------ *)
(** COMMITTED **)
loc inter: invariant True
	when True sync VS do {t_VRP := 0} goto VRP;

loc VRP: invariant t_VRP <= TVRP
	when t_VRP >= TVRP sync VRPac goto Idle;
	(* To prevent blocking *)
	when True sync AP goto VRP;
	when True sync AS goto VRP;
	when True sync Aget goto VRP;
	when True sync PVARPbacktoIdle goto VRP; (*(self loop to avoid deadlocking)*)


end (*VRP*)



(************************************************************)
  automaton RHM
(************************************************************)
synclabs: Aget, AP;

loc AReady: invariant x < Amaxwait
	when True sync AP do {x := 0} goto AReady;
	when x > Aminwait sync Aget do {x := 0} goto AReady;

end (*RHM*)


(************************************************************)
  automaton monitor5a
(*  figure 5a of the paper  *)
(************************************************************)
synclabs: VP, VS, FAC, FER;

loc wait_1st: invariant True
	when True sync VP do {t_5a := 0} goto wait_2nd;
	when True sync VS do {t_5a := 0} goto wait_2nd;

loc wait_2nd: invariant True
	when True sync VP do {x_urgent := 0} goto two_a;
	when True sync VS do {x_urgent := 0} goto two_a;

loc two_a: invariant x_urgent = 0
	when x_urgent = 0 sync FAC do {t_5a := 0} goto wait_2nd;
	when x_urgent = 0 && t_5a > TLRI sync FER do {t_5a := 0} goto monitor5a_BAD;

(* Sink location (stop time) *)
loc monitor5a_BAD: invariant x_urgent = 0

end (* monitor5a *)


(************************************************************)
(* Initial state *)
(************************************************************)

init := {
	discrete =
		(*------------------------------------------------------------
		   INITIAL LOCATION
		  ------------------------------------------------------------*)
		loc[LRI]			:= LRI,
		loc[AVI]			:= Idle,
		loc[URI]			:= URI,
		loc[PVARP]		:= Idle,
		loc[VRP]			:= Idle,
		loc[RHM]			:= AReady,
		loc[monitor5a]	:= wait_1st,
	;

	continuous =
		(*------------------------------------------------------------
		   INITIAL CLOCKS
		  ------------------------------------------------------------*)
		& clk		= 0
		& x			= 0
		& x_urgent	= 0

		& t_AVI		= 0
		& t_LRI		= 0
		& t_PVARP	= 0
		& t_VRP		= 0
		& t_5a		= 0


		(*------------------------------------------------------------
		   PARAMETER CONSTRAINTS
		  ------------------------------------------------------------*)
		& TAVI		>= 0
		& TLRI		>= 0
		& TPVARP	>= 0
		& TVRP		>= 0
		& TURI		>= 0
		& TPVAB		>= 0
		& Aminwait	>= 0
		& Amaxwait	>= 0
		& TLRI < 500
		& TLRI > 400
		& Aminwait <= Amaxwait


		(*------------------------------------------------------------
		   PARAMETER VALUATION
		  ------------------------------------------------------------*)
		(*	& TAVI		= 150
		& TLRI		= 1000
		& TPVARP	= 100
		& TVRP		= 150
		& TURI		= 400
		& TPVAB		= 50*)
	;
}

end